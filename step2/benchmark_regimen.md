## Benchmark regimen

### Problem sizes

By now we try to use the following problem sizes for benchmarking:

- 10
- 20
- 30
- 40
- 50
- 100
- 200
- 300
- 400
- 500
- 600
- 700
- 800

We try to capture some small problem sizes as well as big ones. As the execution time grows with O(n³),
we don't want not greatly surpass a problem size of 800, as it would take too long to get the benchmark
results on our devices. Especially when we want to execute a problem size multiple time in order to get
more reliable results.

Since our benchmarking will be automatized, we can safely add new problem sizes for a clearer picture
of the programs performance.


### Hardware platforms
We will use 3 different laptops:
- lenovo thinkpad: 16 GB RAM, i7-5600U CPU @ 2.6Ghz * 4 cores
- sony vaio: 8 GB RAM, i7-3632QM CPU @ 2.20GHz * 8 cores
- sony vaio: 8 GB RAM, i5-4200U CPU @ 1.60GHz * 4 cores

This will allow us to test our programs with different clock speeds and different number of cores.


### Metrics:
We can use the `time` command in Unix to get CPU and Wall time of the program. With the help of a Python
script calling the `time` command on our executables multiple times, we can generate text
files with the accumulated results. From these results we can then derive statistical metrics like means
or even create plots. For example, currently the Python script executes the given problem sizes on the different
programs 5 times by default, averages these 5 results and saves them in the files and also uses them for the
plots in the end.


### Statistical variation:
We can execute a program multiple times and calculate the mean of execution times as already implied above.
In the problem of matrix multiplication the variation shouldn't be too big,
as every execution has to fully iterate the loops. There is no best or worst case, only the average of O(n³).

### External load:
Of course, there exists the problem of the program taking more time with other programs running in parallel.
A sample solution could be to run the program externally (eg. on the cloud) a or on some VM to minimize the effect.

By using the CPU time instead of the Wall time when doing benchmarks and comparisons between different program versions,
we can measure the actual time the program is running on the CPU. This should minimize the effect of external load.
