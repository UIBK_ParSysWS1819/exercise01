#!/usr/bin/python
import subprocess
import plotly.offline as py
import plotly.graph_objs as go

# guide used https://plot.ly/python/line-charts/

sizes = [10, 20, 30, 50, 100, 110, 150, 200, 220, 270, 300, 350, 400, 450, 500]
# sizes = [10, 100, 500, 1000, 2000]
options ="%E,%K,%U"
build_path = "../cmake-build-debug/"
file_names = ['step1/mmul', 'step3/mmul_seq_opt1', 'step3/mmul_seq_opt2', 'step3/mmul_seq_opt3', 'step3/mmul_seq_opt4', 'step4/mmul_omp']

measure_per_call = 5
results = []

# for each executeable
for file in file_names:
    executable = str.split(file, '/')[1]
    print("currently measuring: " + executable)
    measurements = []
    # measure different sizes
    for m_size in sizes:
        current_times = [0.0, 0.0, 0.0]
        # and measure each size multiple times and average them
        for j in range(0, measure_per_call):
            time_out = str.split(subprocess.getoutput('time -f ' + str(options) + ' ' + str(build_path) + file + ' '
                                                      + str(m_size)), ',')
            print(m_size)
            print(time_out)
            current_times[0] += float(str.split(time_out[0], ':')[1])
            current_times[1] += float(time_out[1])
            current_times[2] += float(time_out[2])
        current_times = [x/measure_per_call for x in current_times]
        print("current size: " + str(m_size) + " with times " + str(current_times))
        measurements.append(dict(
            size=m_size
            , measurements=current_times
        ))
    print(measurements)
    results.append([executable, measurements])
print(results)


def plot_time_option(option):
    """ option corresponds to format parameter of time call
    """
    name_val_pair = []
    for executable_measure in results:
        # print plot for program
        current_program = executable_measure[0]
        # print(str(current_program))
        data_points = []
        # get one kind of measurement
        for val in executable_measure[1]:
            data_points.append(val['measurements'][option])
            # size = val['size']
            # print("size " + str(size) + ' '+ str(data_points))
        # each position corresponds to a size in plot (x-axis)
        # print(current_program + ': ' + str(data_points))
        name_val_pair.append((current_program, data_points))
    return name_val_pair


data = []


def plot_benchmark(plot, time_option):
    for tmp, exe_props in enumerate(plot):
        print("name = "+ str(exe_props[0]))
        print("y = "+ str(exe_props[1]))
        data.append(go.Scatter(
            x = sizes
            , y = exe_props[1]
            , name = exe_props[0] + '_' + str(time_option)
        ))


plot_E = plot_time_option(0)
plot_K = plot_time_option(1)
plot_U = plot_time_option(2)

plot_benchmark(plot_E, 'clock-time')
plot_benchmark(plot_K, 'avg-mem[kB]')
plot_benchmark(plot_U, 'User-CPUs')

# Edit the layout
layout = dict(title='Time per size',
              xaxis=dict(title='size'),
              yaxis=dict(title='time in seconds'),
              )

fig = dict(data=data, layout=layout)
py.plot(fig, filename='benchmark.html')
