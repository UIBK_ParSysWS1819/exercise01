#!/usr/bin/python3

import os
import subprocess
import plotly.offline as py
import plotly.graph_objs as go

# guide used https://plot.ly/python/line-charts/


def benchmark(file_names, problem_sizes, options, build_path, measures_per_call=5):
    results = []

    # for each executeable
    for file in file_names:
        executable = str.split(file, '/')[1]
        print("Currently measuring: " + executable)
        measurements = []

        # measure different sizes
        for m_size in problem_sizes:
            print("Current problem size: " + str(m_size))
            current_times = [0.0, 0.0, 0.0]

            # and measure each size multiple times and average them
            for j in range(0, measures_per_call):
                output = subprocess.getoutput('time -f ' + str(options) + ' ' + str(build_path) + file + ' ' + str(m_size)) # execute command
                time_out = str.split(output, ',')
                #print(time_out)

                current_times[0] += float(time_out[0])
                current_times[1] += float(time_out[1])
                current_times[2] += float(time_out[2])

            current_times = [x / measures_per_call for x in current_times]

            #print("Averaged times for a problem size of " + str(m_size) + ": " + str(current_times))

            measurements.append(dict(problem_size=m_size, measurements=current_times))

        #print("Averaged measurements per size: " + str(measurements))

        results.append([executable, measurements])

    #print(results)

    return results


def plot_time_option(option, results):
    """
        This function separates the values for the given option from the results for each program to be benchmarked
        and return them.

        option corresponds to format parameter of time call
    """
    name_val_pair = []
    for executable_measure in results:
        # print plot for program
        current_program = executable_measure[0]

        # print(str(current_program))
        data_points = []

        # get one kind of measurement
        for val in executable_measure[1]:
            data_points.append(val['measurements'][option])

        # each position corresponds to a size in plot (x-axis)
        name_val_pair.append((current_program, data_points))

    return name_val_pair


def plot_benchmark(plot, problem_sizes, time_option):
    data = []

    for tuple in plot:
        executable = tuple[0]
        time_option_values = tuple[1]

        #print("y-values for benchmark of '" + executable + "' with time option '" + time_option + "': " + str(time_option_values))

        data.append(go.Scatter(
            x = problem_sizes,
            y = time_option_values,
            name = executable + '_' + str(time_option)
        ))

    return data


def output_diagrams(data, layout, filename):
    fig = dict(data=data, layout=layout)
    py.plot(fig, filename=filename)


def write_results_to_file(results, output_path="."):
    for result in results:
        output_filename = output_path + "/" + result[0] + "_results.csv"
        measurements = result[1]

        if os.path.exists(output_filename): # remove file if it already exists at specified location
            os.remove(output_filename)

        file = open(output_filename, "a")
        file.write("problem-size;Elapsed-time;CPU-secs-kernel;CPU-secs-user\n")

        for measurement in measurements:
            file.write(str(measurement['problem_size']) + ";" + str(measurement['measurements'][0]) + ";" + str(measurement['measurements'][1]) + ";" + str(measurement['measurements'][2]) + "\n")

        file.close()

    print("Results written to files!")


def main():
    #problem_sizes = [10, 20, 30, 50, 100, 110, 150]
    problem_sizes = [10, 20, 30, 40, 50, 100, 200, 300, 400, 500, 600, 700, 800]

    # options = "%e,%K,%U"  # Elapsed-time[sec] | Average total memory use[kB] | Total number of CPU seconds used (user mode)
    options = "%e,%S,%U"  # Elapsed real time[sec] | Total number of CPU seconds used (kernel mode) | Total number of CPU seconds used (user mode)

    build_path = "../cmake-build-debug/"
    file_names = ['step1/mmul', 'step3/mmul_seq_opt1', 'step3/mmul_seq_opt2', 'step3/mmul_seq_opt3', 'step3/mmul_seq_opt4', 'step4/mmul_omp']

    measures_per_call = 5

    # Do benchmarking and retrieved the results with given information
    results = benchmark(file_names, problem_sizes, options, build_path, measures_per_call)

    # Output result values in files
    output_path = "./output"
    write_results_to_file(results, output_path)

    # Separate results for time options
    # 0 = elapsed-time, 1 = CPU-secs-kernel, 2 = CPU-secs-user
    plot_e = plot_time_option(0, results)
    plot_S = plot_time_option(1, results)
    plot_U = plot_time_option(2, results)

    # format the data to be used for plotting
    data_e = plot_benchmark(plot_e, problem_sizes, 'elapsed-time')
    data_S = plot_benchmark(plot_S, problem_sizes, 'CPU-secs-kernel')
    data_U = plot_benchmark(plot_U, problem_sizes, 'CPU-secs-user')

    # set layouts for plotting
    layout_for_times = dict(title='Time per size',
                            xaxis=dict(title='problem size'),
                            yaxis=dict(title='time in seconds')
                            )

    #layout_for_memory = dict(title='Memory per size', xaxis=dict(title='size'), yaxis=dict(title='kB'))

    # plot results and save them in files
    output_diagrams(data_e, layout_for_times, output_path + "/" + 'benchmark_e.html')
    output_diagrams(data_S, layout_for_times, output_path + "/" + 'benchmark_S.html')
    output_diagrams(data_U, layout_for_times, output_path + "/" + 'benchmark_U.html')


main()


"""
    Format options for time -f:
    C - Name and command line arguments used
    D - Average size of the process's unshared data area in kilobytes
    E - Elapsed time in a clock format
    F - Number of page faults
    I - Number of file system inputs by the process
    K - Average total memory use of the process in kilobytes
    M - Maximum resident set size of the process during the lifetime in Kilobytes
    O - Number of file system outputs by the process
    P - Percentage of CPU that the job received
    R - Number of minor or recoverable page faults
    S - Total number of CPU seconds used by the system in kernel mode
    U - Total number of CPU seconds used by user mode
    W - Number of times the process was swapped out of main memory
    X - Average amount of shared text in the process
    Z - System's page size in kilobytes
    c - Number of times the process was context-switched
    e - Elapsed real time used by the process in seconds
    k - Number of signals delivered to the process
    p - Average unshared stack size of the process in kilobytes
    r - Number of socket messages received by the process

    s - Number of socket messages sent by the process
    t - Average resident set size of the process in kilobytes
    w - Number of time the process was context-switched voluntarily
    x - Exit status of the command
"""