#include <vector>
#include <cstdlib>
#include <numeric>

using Matrix = std::vector<std::vector<double>>;

// initializes a square identity matrix of size n x n
Matrix id(unsigned n) {
    Matrix res;
    res.resize(n);
    for(unsigned i=0; i<n; i++) {
        res[i].resize(n);
        for(unsigned j=0; j<n; j++) {
            res[i][j] = (i == j) ? 1 : 0;
        }
    }
    return res;
}

// returns a transposed version of the given matrix
Matrix transpose(const Matrix& a) {
    unsigned n = a.size();
    Matrix c = id(n);
    for(unsigned i=0; i<n; ++i) {
        for(unsigned j=0; j<n; ++j) {
            c[i][j] = a[j][i];
        }
    }
    return c;
}

// computes the product of two matrices
Matrix operator*(const Matrix& a, const Matrix& b) {
    unsigned n = a.size();
    Matrix c = id(n);
    for(unsigned i=0; i<n; ++i) {
        for(unsigned j=0; j<n; ++j) {
            // https://en.cppreference.com/w/cpp/algorithm/inner_product
            // std::inner_product(begin_first_range, end_first_range, begin_second_range, initial_value);
            c[i][j] = std::inner_product(a[i].begin(), a[i].end(), b[j].begin(), 0);
        }
    }
    return c;
}


int main(int argc, char** argv) {

    if(argc!=2) return EXIT_FAILURE;
    unsigned n = atoi(argv[1]);
    if(n==0) return EXIT_FAILURE;

    // create two matrices
    auto a = id(n);
    a[0][0] = 42;
    auto b = transpose(id(n));

    // compute the product
    auto c = a * b;

    // check that the result is correct
    return (c == a) ? EXIT_SUCCESS : EXIT_FAILURE;
}
