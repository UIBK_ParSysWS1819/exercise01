#include <vector>
#include <cstdlib>
#include <cmath>

using Matrix = std::vector<double>;

// initializes a square identity matrix of size n x n
Matrix id(unsigned n) {
    Matrix res;
    res.resize(n * n);

    for(unsigned i = 0; i < n; i++) {
        unsigned int offset_i = i * n;        // save redundant operation outside the loop to do it just once

        for(unsigned j = 0; j < n; j++) {
            res[offset_i + j] = (i == j) ? 1 : 0;
        }
    }

    return res;
}

// computes the product of two matrices
Matrix operator*(const Matrix& a, const Matrix& b) {
    /* solution based on first one */
    unsigned n = sqrt(a.size());
    Matrix c = id(n);

    for(unsigned i = 0; i < n; ++i) {
        unsigned int offset_i = i * n;        // save redundant operation outside the loop to do it just once

        for(unsigned j = 0; j < n; ++j) {
            c[offset_i + j] = 0;
            for(unsigned k = 0; k < n; ++k) {
                c[offset_i + j] += a[offset_i + k] * b[k * n + j];
            }
        }
    }

    return c;
}


int main(int argc, char** argv) {
	
	if(argc != 2) return EXIT_FAILURE;
	unsigned n = atoi(argv[1]);
	if(n == 0) return EXIT_FAILURE;

	// create two matrices
    auto a = id(n);
    a[0] = 42;
    auto b = id(n);

	// compute the product
	auto c = a * b;

	// check that the result is correct
	return (c == a) ? EXIT_SUCCESS : EXIT_FAILURE;
}
