## Step 3

### General

We did 4 sequential optimizations on the original code in `mmul.cpp`, each one saved in a
separate source file:

- The first one uses a 1D vector, instead of the original 2D vector (`mmul_seq_opt1.cpp`). This means the index
for accessing an element now looks like `i * n + j` with `n` being the input size as usual. This means that we can omit
the `resize` function calls in the loop for initialization, but instead we have to provide `n*n` instead of `n` as argument.
- The second extends the first version by avoiding redundant (unnecessary) operations done in the loops (`mmul_seq_opt2.cpp`).
These operations are now done before the loop execution and just reused in each iteration of the loop.
- The third version transposes the second matrix in order to increase cache hits. The second matrix can then be traversed
row by row.
- The forth version differs from the third in the vector multiplication part of the matrix multiplication.
In addition instead of doing the vector multiplication with a for loop we utilize the `std::inner_product` function
that takes care of the multiplication for us.

We could even go on as to combine the transposition and the flattening of version 1 and 3.

### Benchmarking results

We currently measure the total elapsed real time in seconds and the total number of CPU seconds used user mode for measuring.
We tried an additional measuring like total number of CPU seconds used kernel mode, but without meaningful results and thus
omitted them here.
As already written in the benchmarking regimen, we do several runs on our sequential
optimizations and average them. These values are then used for plotting. We have a plot for each of the 2 measures
denoted above containing a graph for each program with respect to the problem sizes.

The script and all output files are located under /scripts/output in this project.

The following results come from a benchmark executed on this machine:
sony vaio: 8 GB RAM, i5-4200U CPU @ 1.60GHz * 4 cores

We also executed the benchmark with the other laptops, but all results are pretty similar. So we just explained it here by using
one device.

Note that here also the benchmark of step 4 is included, because we plotted all programs in one diagram.

#### Elapsed Time
First, take a look at the plot `benchmark_e.html`.
- the original version is the slowest of all
- the sequential versions are all better than the original one, but slower than the OpenMP version
- of the sequential version, the third one is the fastest, whereas the second one is the slowest
- OpenMP version is the fastest of all
- the original version gets much steeper with every step than the others

### CPU seconds(user mode)
Look at the plot of `benchmark_U.html`.
- Similar as the elapsed version
- ordering of the sequential version is the same (third fastest, second slowest)
- This time the OpenMP version is the slowest, even slower than the original version and the sequential versions. The
reason for this is that, the OpenMP version utilises multiple CPU cores simultaneously, but for a shorter amount of time.
This measurement adds all times each process runs on one CPU core. The resulting time is thus greater than the actual time
the program ran. This total of CPU seconds is greater than some sequential version, as OpenMP has to handle communication
and synchronization internally, which adds CPU time.
- OpenMP version gets much steeper every step
- Original version still slower than the sequential optimizations