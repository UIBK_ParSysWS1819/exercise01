## Step 4

### OpenMP Pragmas
- `#pragma omp parallel`: Forms a team of threads and starts parallel execution

- `#pragma omp for`: Specifies that the iterations of associated loops will be executed in parallel by threads
 in the team in the context of their implicit tasks

- `#pragma omp parallel for`: Shortcut for specifying a parallel construct containing one or more associated
loops and no other statements

### General

Different to step 3 we don't use the sequentially optimized version for benchmarking. Here the benchmarking is
done on the original version of the code, but we used the OpenMP pragmas for the outer loops of the initialization
and multiplication. See lines 12 and 28 in `mmul_omp.cpp`.

### Benchmarking results
We currently measure the total elapsed real time in seconds and the total number of CPU seconds used user mode for measuring.
We tried an additional measuring like total number of CPU seconds used kernel mode, but without meaningful results and thus
omitted them here. As already written in the benchmarking regimen, we do several runs and average them.
These values are then used for plotting. We have a plot for each of the 2 measures
denoted above containing a graph for each program with respect to the problem sizes.

The script and all output files are located under /scripts/output in this project.

The following results come from a benchmark executed on this machine:
sony vaio: 8 GB RAM, i5-4200U CPU @ 1.60GHz * 4 cores

We also executed the benchmark with the other laptops, but all results are pretty similar. So we just explained it here by using
one device.

The benchmarking itself is included in step 3 with the other results from there, because we plotted all programs together.