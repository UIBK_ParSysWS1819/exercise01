# Exercise 1

## Step 1
Study the usage of the **gprof** tool. Apply it to the matrix multiplication example and study the report. 
(Also commit it to your git repo)
Try this with both a version compiled using `-O0` and one with `-O3` and observe the differences.
___

### Usage of the gprof tool
Needs an additional flags for compilation:
- -pg    generate profile information for **gprof**
- -g     debugging informations (needed for further features like line-number and annotations)

Can be then called with:    `gprof mmul gmon.out > profiling.txt`    

Other useful flags we used:
- -b : brief (remove explanations from the gprof result)
- -A -x -l  Results in annotated code. Shows more detailed explanation of code lines.
    - -A  Annotated Source Listing (requires -g during compilation)
    - -l  line-by-line profiling
    - -x  all-lines

With gprof the user can see how much time is spent executing each function and thus it is a simple way to find the most time consuming functions.
    
### Gprof output in general
Flat profile:
```
%   cumulative   self              self     total           
time   seconds   seconds    calls  Ts/call  Ts/call  name    
0.00      0.00     0.00        1     0.00     0.00  id(unsigned int)
```
You can read the time spent inside the function, the number of calls etc. per function including system/library functions.

Call graph:
```
index % time    self  children    called     name
                0.00    0.00      30/30          id(unsigned int) [10]
[8]      0.0    0.00    0.00      30         std::vector<double, std::allocator<double> >::_M_default_append(unsigned long) [8]
```

One block per function call:
    function with the index was called. Rest is in tree structure with the parents beeing above it and the children underneath.

In this example the resize function of `mmul.cpp` was called 30 time inside the id function with matrix size 10x10.

If there would be another function that calls the id function the output might look something like this:
```cpp
void foo(int n){
    auto a2 = id(n);
}
```

```
                0.00    0.00       1/2           operator*(std::vector<std::vector<double, std::allocator<double> >, std::allocator<std::vector<double, std::allocator<double> > > > const&, std::vector<std::vector<double, std::allocator<double> >, std::allocator<std::vector<double, std::allocator<double> > > > const&) [13]
                0.00    0.00       1/2           foo(int) [11]
[10]     0.0    0.00    0.00       2         id(unsigned int) [10]
                0.00    0.00      50/50          std::vector<double, std::allocator<double> >::_M_default_append(unsigned long) [8]
                0.00    0.00       4/4           std::vector<std::vector<double, std::allocator<double> >, std::allocator<std::vector<double, std::allocator<double> > > >::_M_default_append(unsigned long) [9]

```

Id is called twice, once by foo and once by the operator function.


### Report -O0
In [profile-data-O0](./profile-data-O0_1000.txt) you can see the tables in the same structure as above.
In contrast to O3 you can see that there are much more calls to the included library functions, that can be optimized and 
aren't present as in the O3 output.

### Report -O3
In [profile-data-O3](./profile-data-O3_1000.txt) you can see the tables in the same structure as above.
This code was optimized resulting in much less function calls, and thus less time spent for execution.

### Call interpretations
Some interesting lines:
O0 100x100 Matrix:
```
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  ms/call  ms/call  name    

12.50      0.04     0.01  2000000     0.00     0.00  std::vector<std::vector<double, std::allocator<double> >, std::allocator<std::vector<double, std::allocator<double> > > >::operator[](unsigned long) const
...
 0.00      0.00     0.00        3     0.00     0.00  id(unsigned int)

```

#### Example 1

Code line 27:

    c[i][j] += a[i][k] * b[k][j];

Reading out of the 2D vectors a & b: 
- 2 x 100 x 100 x 100 = 2.000.000 calls (being each x100 for each for loop)

#### Example 2

```
                0.00    0.00       1/3           operator*(std::vector<std::vector<double, std::allocator<double> >, std::allocator<std::vector<double, std::allocator<double> > > > const&, std::vector<std::vector<double, std::allocator<double> >, std::allocator<std::vector<double, std::allocator<double> > > > const&) (mmul.cpp:22 @ d0d) [488]
                0.00    0.00       1/3           main (mmul.cpp:42 @ ea8) [9]
                0.00    0.00       1/3           main (mmul.cpp:44 @ ee3) [11]
[101]    0.0    0.00    0.00       3         id(unsigned int) (mmul.cpp:7 @ bba) [101]
```

id gets called 3 times:
- 2 times inside main in line 42 and 44
- 1 time in the operator for initialization of c in line 22.
